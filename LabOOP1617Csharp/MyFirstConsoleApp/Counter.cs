﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstConsoleApp
{
    class Counter
    {
        private int val;
        public Counter(int v)
        {
            this.val = v;
        }
        public Counter() : this(0)
        {

        }

        public virtual void Inc()
        {
            val++;
        }

        public int Value
        {
            get { return val; }
            // set { val = value; }
        }
    }

    class ExtendedCounter : Counter
    {
        private const int max = 10;

        public ExtendedCounter() : base()
        {

        }

        public ExtendedCounter(int v) : base(v)
        {

        }

        public override void Inc()
        {
            if (Value < max)
                base.Inc();
        }
    }
}
