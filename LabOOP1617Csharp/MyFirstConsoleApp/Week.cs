﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstConsoleApp
{
    class Week
    {
        string[] days = { "Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom" };

        public Week()
        {

        }

        public string this[int d]
        {
            get {
                if (d >= 0 && d < days.Length)
                    return days[d];
                else
                    return "";
            }
        }
    }
}
