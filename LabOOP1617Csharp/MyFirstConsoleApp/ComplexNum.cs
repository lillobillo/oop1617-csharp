﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstConsoleApp
{
    class ComplexNum
    {
        private double re, im;

        public ComplexNum(double re, double im)
        {
            this.re = re;
            this.im = im;
        }

        public double Re
        {
            get { return re; }
        }

        public double Im
        {
            get { return im; }
        }

        public override string ToString()
        {
            return this.Re + "+" + this.Im + "i";
        }

        public static ComplexNum operator+(ComplexNum c1, ComplexNum c2)
        {
            return new ComplexNum(c1.Re + c2.Re, c1.Im + c2.Im);
        }
    }
}
